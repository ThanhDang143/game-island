﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCheckCenter : MonoBehaviour
{
    public Vector2[] pos;
    float dis;

    void Start()
    {
        pos = new Vector2[transform.childCount];
        dis = Mathf.Abs(transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition.x - transform.GetChild(1).GetComponent<RectTransform>().anchoredPosition.x);
        for (int i = 0; i < transform.childCount; i++)
        {
            pos[i] = new Vector2(GetComponent<RectTransform>().position.x + (dis * (i + 1)), GetComponent<RectTransform>().position.y);
            Debug.Log(pos[i]);
        }
        Debug.Log(transform.GetChild(0).GetComponent<RectTransform>().pivot.x);
        Debug.Log(transform.GetChild(1).GetComponent<RectTransform>().pivot.x);
    }

}
