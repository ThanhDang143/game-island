﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlCoin : MonoBehaviour
{
    public static ControlCoin ins;
    public int coin;
    public int initialCoin;
    public Text txtCoin;

    void Start()
    {
        ins = this;
        coin = PlayerPrefs.GetInt("coin", 50);
        initialCoin = coin;
        txtCoin.text = coin.ToString();
    }

    public void UpCoin(int coinPlus)
    {
        ControlCoin.ins.initialCoin = ControlCoin.ins.coin;
        ControlCoin.ins.coin += coinPlus;
        PlayerPrefs.SetInt("coin", this.coin);
    }

    public IEnumerator IEShowCoin()
    {
        if (this.initialCoin < this.coin)
        {
            for (int i = this.initialCoin; i <= this.coin; i++)
            {
                ControlCoin.ins.txtCoin.text = i.ToString();
                yield return new WaitForSeconds(0.01f);
            }
        }
        else if (this.initialCoin > this.coin)
        {
            for (int i = this.initialCoin; i >= this.coin; i--)
            {
                ControlCoin.ins.txtCoin.text = i.ToString();
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
}
