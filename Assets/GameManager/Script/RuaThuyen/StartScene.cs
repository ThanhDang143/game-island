﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour
{
    [SerializeField] private GameObject nangTau;
    [SerializeField] ControlTools controlTools;
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string move;
    [SerializeField] GameObject[] effect;
    [SerializeField] GameObject endScreen;
    [SerializeField] GameObject solidBlack;
    [SerializeField] Sprite[] imgEnd;

    void Start()
    {
        endScreen.SetActive(false);
        endScreen.transform.GetChild(0).GetComponent<BoxCollider2D>().enabled = false;
        StartCoroutine(IEStart());
    }
    void Update()
    {
        if (controlTools.done == 4)
        {
            controlTools.done++;
            StartCoroutine(IEDone());
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("May") && !other.CompareTag("Song"))
        {
            effect[0].SetActive(false);
            effect[1].SetActive(false);
            skeletonAnimation.AnimationState.ClearTracks();
            transform.DOMoveY(transform.position.y + 1.1f, 1.075f);
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private IEnumerator IEStart()
    {
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("DonRac"));
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 1f);
        effect[0].SetActive(true);
        effect[1].SetActive(true);
        transform.DOMoveX(transform.position.x + 12.75f, 7f);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        yield return new WaitForSeconds(7f);
        nangTau.transform.DOMoveY(nangTau.transform.position.y + 2.1f, 1.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    private IEnumerator IEDone()
    {
        yield return new WaitForSeconds(1.5f);
        nangTau.transform.DOMoveY(nangTau.transform.position.y - 2.1f, 1.5f);
        transform.DOMoveY(transform.position.y - 1.1f, 1.075f);
        yield return new WaitForSeconds(1.5f);
        effect[0].SetActive(true);
        effect[1].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 12.75f, 7f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        endScreen.transform.GetChild(0).DOScale(new Vector3(2, 2, 2), 0);
        yield return new WaitForSeconds(4f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
        endScreen.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = imgEnd[ControlGame.ins.idStep];
        for (int i = 2; i < endScreen.transform.childCount; i++)
        {
            endScreen.transform.GetChild(i).gameObject.SetActive(false);
        }
        endScreen.SetActive(true);
        endScreen.transform.GetChild(0).DOScale(Vector3.one, 1.25f);
        FindObjectOfType<AudioManager>().PlaySound("CameraShutter", false);
        yield return new WaitForSeconds(1.2f);
        endScreen.transform.GetChild(0).DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.5f);
        ControlCoin.ins.UpCoin(5);
        StartCoroutine(ControlCoin.ins.IEShowCoin());
        yield return new WaitForSeconds(0.5f);
        FindObjectOfType<AudioManager>().PlaySound("Win", false);
        for (int i = 2; i < endScreen.transform.childCount; i++)
        {
            endScreen.transform.GetChild(i).gameObject.SetActive(true);
        }
        HintRuaThuyen.waitHint = -2000f;
        SaveGame();
        yield return new WaitForSeconds(5f);
        endScreen.transform.GetChild(0).GetComponent<BoxCollider2D>().enabled = true;
        while (true)
        {
            endScreen.transform.GetChild(0).DOScale(new Vector3(1.25f, 1.25f, 1.25f), 1f);
            yield return new WaitForSeconds(1f);
            endScreen.transform.GetChild(0).DOScale(new Vector3(1.2f, 1.2f, 1.2f), 1f);
            yield return new WaitForSeconds(1f);
        }
    }

    private void SaveGame()
    {
        if (ControlGame.ins.idStep >= PlayerPrefs.GetInt("Step" + MoveScreen.ins.selectedLevel))
        {
            PlayerPrefs.SetInt("Step" + MoveScreen.ins.selectedLevel, PlayerPrefs.GetInt("Step" + MoveScreen.ins.selectedLevel) + 1);
        }
    }
}
