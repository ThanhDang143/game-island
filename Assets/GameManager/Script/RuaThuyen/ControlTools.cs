﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;
public class ControlTools : MonoBehaviour
{
    public bool isUsing;
    public int done;
    public GameObject[] targetOfTools;
    public GameObject[] tools;

    private void Start()
    {
        for (int i = 0; i < tools.Length; i++)
        {
            tools[i].transform.position = new Vector3(4f, 4.5f, 0f);
            targetOfTools[i].transform.position = new Vector3(4f, 6f, 0f);
            tools[i].GetComponent<AudioSource>().enabled = false;
        }
        StartCoroutine(IEStart());
    }

    IEnumerator IEStart()
    {
        yield return new WaitForSeconds(7f);
        StartCoroutine(targetOfTools[0].GetComponent<Target>().IEEnable(2f));
    }
}
