﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Target : MonoBehaviour
{
    [SerializeField] ControlTools controlTools;
    [SerializeField] GameObject particle;
    Tool getTool;
    AnimTramRua getAnimTramRua;
    public int toolUsing;

    private void Start()
    {
        getTool = particle.transform.parent.parent.GetComponent<Tool>();
        getAnimTramRua = FindObjectOfType<AnimTramRua>().GetComponent<AnimTramRua>();
        particle.SetActive(false);
        toolUsing = 0;
        GetComponent<BoxCollider2D>().enabled = false;
    }
    public void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            getAnimTramRua.Play();
            getTool.PlayAnim();
            getTool.GetComponent<AudioSource>().enabled = true;
            HintRuaThuyen.ins.hand.SetActive(false);
            HintRuaThuyen.waitHint = 0f;
            particle.SetActive(true);
            controlTools.isUsing = true;
            transform.DOMove(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -5f), 0f);
        }
    }

    public void OnMouseDrag()
    {
        if (Time.timeScale != 0)
        {
            HintRuaThuyen.waitHint = 0f;
            if (toolUsing < 2)
            {
                transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z);
            }
        }
    }

    public void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            getAnimTramRua.Stop();
            getTool.GetComponent<AudioSource>().enabled = false;
            getTool.StopAnim();
            HintRuaThuyen.ins.hand.SetActive(false);
            HintRuaThuyen.waitHint = 0f;
            if (toolUsing > 2)
            {
                // StartCoroutine(IEDisable());
                return;
            }
            else
            {
                particle.SetActive(false);
                controlTools.isUsing = false;
                GetComponent<BoxCollider2D>().enabled = false;
                StartCoroutine(IEEnable(1f));
            }
        }
    }

    void Update()
    {
        if (toolUsing == 1)
        {
            if (gameObject == controlTools.targetOfTools[0])
            {
                StartCoroutine(IEEnable(8f));
            }
            else
            {
                StartCoroutine(IEEnable(2f));
            }
            toolUsing = 0;
        }
        else if (toolUsing == 2)
        {
            StartCoroutine(IEDisable());
            toolUsing = 3;
        }
    }

    public IEnumerator IEEnable(float waitTime)
    {
        HintRuaThuyen.ins.hand.SetActive(false);
        HintRuaThuyen.waitHint = 0;
        particle.SetActive(false);
        transform.DOMove(new Vector3(3f, 1f, -5), waitTime);
        yield return new WaitForSeconds(waitTime);
        GetComponent<BoxCollider2D>().enabled = true;
    }

    public IEnumerator IEDisable()
    {
        particle.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        controlTools.isUsing = false;
        transform.DOMove(new Vector3(4f, 6f, 0f), 2f);
        yield return new WaitForSeconds(2f);
    }
}
