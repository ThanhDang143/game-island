﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HintRuaThuyen : MonoBehaviour
{
    public static HintRuaThuyen ins;
    public GameObject hand;
    private Animator anim;
    public AnimationClip tay;
    public AnimationClip tayx;
    public static float waitHint = 0f;
    public GameObject startHintPos;
    public GameObject endHintPos;

    void Start()
    {
        ins = this;
        waitHint = 0;
        anim = hand.GetComponent<Animator>();
        ControlGame.ins.hideHint = () => hand.SetActive(false);
        hand.SetActive(false);
    }

    void Update()
    {
        waitHint += Time.deltaTime;

        if (waitHint > 9f)
        {
            StartCoroutine(IEHint(startHintPos, endHintPos));
            waitHint = 4f;
        }
    }

    public IEnumerator IEHint(GameObject start, GameObject end)
    {
        hand.transform.position = start.transform.position - new Vector3(-0.35f, 0.75f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.35f);
        yield return new WaitForSeconds(0.75f);
        hand.transform.DOMove(end.transform.position - new Vector3(-0.35f, 0.75f, 0f), 2f);
        yield return new WaitForSeconds(2.5f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.35f);
        yield return new WaitForSeconds(0.35f);
        hand.SetActive(false);
    }
}
