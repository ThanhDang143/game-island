﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using DG.Tweening;

public class Tool : MonoBehaviour
{
    [SerializeField] Transform target;
    public SkeletonAnimation skeletonAnimation;
    [SerializeField] [SpineBone()] private string boneTarget;
    [SerializeField] private Bone bone;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string mix;

    void Start()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        bone = skeletonAnimation.Skeleton.FindBone(boneTarget);
        skeletonAnimation.UpdateLocal += SkeletonAnimation_UpdateLocal;
    }

    void Update()
    {

        if (Mathf.Abs(transform.position.x - target.position.x) > 2f)
        {
            transform.DOMoveX(target.transform.position.x, 1.5f);
        }
    }

    public void PlayAnim()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true);
    }
    public void StopAnim()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
    }

    private void SkeletonAnimation_UpdateLocal(ISkeletonAnimation animated)
    {
        var localPosition = transform.InverseTransformPoint(target.position);
        bone.SetLocalPosition(localPosition);
    }
}
