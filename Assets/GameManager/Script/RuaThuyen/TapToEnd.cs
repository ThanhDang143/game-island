﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TapToEnd : MonoBehaviour
{
    private void OnMouseUp()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        NextStep();
    }

    private void NextStep()
    {
        ManagerAds.Ins.ShowInterstitial();
        if (ControlGame.ins.idStep >= 9)
        {
            SceneManager.LoadScene("StartMenu");
        }
        else if (ControlGame.ins.idStep == 0)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 0;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 1)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 1;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 2)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 4;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 3)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 1;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 4)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 4;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 5)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 1;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 6)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 3;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 7)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 5;
            SceneManager.LoadScene("LapThuyen");
        }
        else if (ControlGame.ins.idStep == 8)
        {
            ControlGame.ins.idStep++;
            ControlGame.ins.idThuyen = 2;
            SceneManager.LoadScene("LapThuyen");
        }
    }
}
