﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class AnimTramRua : MonoBehaviour
{
    public static AnimTramRua ins;
    public GameObject[] bongBongEffect;
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string two;
    [SpineAnimation()] public string mix;

    void Start()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        bongBongEffect[1].GetComponent<ParticleSystem>().Stop();
    }

    public void Play()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true);
        skeletonAnimation.AnimationState.SetAnimation(1, two, true);
        bongBongEffect[0].GetComponent<ParticleSystem>().Stop();
        bongBongEffect[1].GetComponent<ParticleSystem>().Play();
    }

    public void Stop()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        skeletonAnimation.AnimationState.ClearTrack(1);
        bongBongEffect[1].GetComponent<ParticleSystem>().Stop();
        bongBongEffect[0].GetComponent<ParticleSystem>().Play();
    }
}
