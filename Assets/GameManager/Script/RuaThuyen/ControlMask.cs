﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMask : MonoBehaviour
{
    public GameObject[] mask;
    private void Start() {
        mask[0].SetActive(true);
        mask[1].SetActive(true);
        mask[2].SetActive(false);
        mask[3].SetActive(false);
    }
}
