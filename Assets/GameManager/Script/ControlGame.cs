﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ControlGame : MonoBehaviour
{
    public static ControlGame ins;
    public int idThuyen;
    public int idStep;
    public GameObject pauseMenu;
    public GameObject btnPause;
    public GameObject btnCoin;
    public GameObject solidBlack;
    public int coin;
    public Camera cam;
    public Action hideHint;

    private void Awake()
    {
        if (ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }

        if (!PlayerPrefs.HasKey(KeySaveGame.musicsStatus))
        {
            PlayerPrefs.SetInt(KeySaveGame.musicsStatus, 1);
        }

        if (!PlayerPrefs.HasKey(KeySaveGame.soundsEffectStatus))
        {
            PlayerPrefs.SetInt(KeySaveGame.soundsEffectStatus, 1);
        }
    }
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        pauseMenu.SetActive(false);
        if (btnPause == null) { return; }
        else if (SceneManager.GetActiveScene().name == "StartMenu")
        {
            btnPause.SetActive(false);
        }
        else
        {
            btnPause.SetActive(true);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F10))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("RESET SAVE GAME!!!");
        }
        if (Input.GetKeyDown(KeyCode.F11))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        if (transform.GetChild(0).GetComponent<Canvas>().worldCamera == null)
        {
            transform.GetChild(0).GetComponent<Canvas>().worldCamera = cam;
        }
    }

    public void BtnPause()
    {
        hideHint?.Invoke();
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void BtnContinue()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void BtnHome()
    {
        ManagerAds.Ins.ShowInterstitial();
        StartCoroutine(IEHomeBtn());
    }

    public void BtnRestart()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static bool OnceClick()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.touchCount >= 2)
                return true;
        }
        return false;
    }

    IEnumerator IEHomeBtn()
    {
        Time.timeScale = 1;
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1f, 0.5f);
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        yield return new WaitForSeconds(0.5f);
        pauseMenu.SetActive(false);
        btnPause.SetActive(false);
        SceneManager.LoadScene("StartMenu");
    }
}
