﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Spine.Unity;
using Spine;
using UnityEngine.UI;

public class End : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject shipPart;
    [SerializeField] GameObject shipsShadowPart;
    [SerializeField] GameObject ship;
    [SerializeField] GameObject frame;
    [SerializeField] GameObject shipsFrame;
    [SerializeField] GameObject BG;
    [SerializeField] SkeletonAnimation skeletonAnimation;
    public string mix;
    public string move1;
    public int completedPart = 0;
    public int totalPart = 5;

    void Start()
    {
        shipPart.SetActive(true);
        shipsShadowPart.SetActive(true);
        ship.SetActive(false);
    }

    public void CheckComplete()
    {
        completedPart++;
        if (completedPart != totalPart) return;
        StartCoroutine(IEComplete());
    }

    private IEnumerator IEComplete()
    {
        yield return new WaitForSeconds(0.75f);
        shipPart.SetActive(false);
        shipsShadowPart.SetActive(false);
        ship.SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true);
        frame.transform.DOMoveX(frame.transform.position.x - 4f, 0.75f);
        shipsFrame.transform.DOMoveY(shipsFrame.transform.position.y - 2f, 0.75f);
        ship.transform.DOMoveY(ship.transform.position.y - 1f, 0.4f);
        yield return new WaitForSeconds(0.75f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        skeletonAnimation.AnimationState.SetAnimation(1, move1, true);
        ship.transform.DOMoveX(ship.transform.position.x + 19f, 7f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("DoXang");
    }
}
