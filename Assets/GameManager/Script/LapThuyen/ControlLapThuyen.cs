﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlLapThuyen : MonoBehaviour
{
    [SerializeField] GameObject[] ship;
    [SerializeField] GameObject[] shipPart;

    void Start()
    {
        ControlGame.ins.btnPause.SetActive(true);
        for (int i = 0; i < ship.Length; i++)
        {
            ship[i].SetActive(ControlGame.ins.idThuyen == i);
            shipPart[i].SetActive(ControlGame.ins.idThuyen == i);
        }
        ControlGame.ins.cam = Camera.main;
    }
}
