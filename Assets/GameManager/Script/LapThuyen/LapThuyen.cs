﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LapThuyen : MonoBehaviour
{
    [SerializeField] End end;
    [SerializeField] GameObject endObj;
    public int id;
    public ShipSlot shipSlot;
    Vector3 initialPos;
    Vector3 initialScale;
    int initialOrderInLayer;
    int tempOrderInLayer = 20;
    SpriteRenderer orderInLayer;
    public static LapThuyen ins;

    void Start()
    {
        ins = this;
        orderInLayer = GetComponent<SpriteRenderer>();
        initialOrderInLayer = GetComponent<SpriteRenderer>().sortingOrder;
        initialScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        ScaleObj();
    }

    private void OnMouseDrag()
    {
        if (Time.timeScale != 0)
        {
            if (ControlGame.OnceClick()) return;
            // StartCoroutine(IEOnMouseDrag());
            HinLapThuyen.waitHint = 0f;
            HinLapThuyen.ins.hand.SetActive(false);
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, initialPos.z);
        }
    }

    private void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            FindObjectOfType<AudioManager>().PlaySound("Keo", false);
            if (ControlGame.OnceClick()) return;
            HinLapThuyen.waitHint = 0f;
            HinLapThuyen.ins.hand.SetActive(false);
            orderInLayer.sortingOrder = tempOrderInLayer;
            shipSlot.indexMove = id;
            initialPos = transform.position;
            transform.DOMove(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, initialPos.z), 0.05f);
        }
    }

    public void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            FindObjectOfType<AudioManager>().PlaySound("Tha", false);
            HinLapThuyen.waitHint = 0f;
            HinLapThuyen.ins.hand.SetActive(false);
            if (Mathf.Abs(transform.position.x - endObj.transform.position.x) <= 1f && Mathf.Abs(transform.position.y - endObj.transform.position.y) <= 1f)
            {
                HinLapThuyen.waitHint = 0f;
                orderInLayer.sortingOrder = initialOrderInLayer;
                GetComponent<BoxCollider2D>().enabled = false;
                transform.DOMove(endObj.transform.position, 0.75f);
                shipSlot.MoveToEmptySlot();
                end.CheckComplete();
            }
            else
            {
                HinLapThuyen.waitHint = 0f;
                StartCoroutine(IEFixInitialPos());
            }
        }
    }

    private void ScaleObj()
    {
        if (transform.position.x >= -4f)
        {
            transform.DOScale(Vector3.one, 0.75f);
        }
        else if (transform.position.x < -4f)
        {
            transform.DOScale(initialScale, 0.75f);
        }
    }

    private IEnumerator IEFixInitialPos()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOMove(initialPos, 0.75f);
        yield return new WaitForSeconds(0.8f);
        GetComponent<BoxCollider2D>().enabled = true;
        orderInLayer.sortingOrder = initialOrderInLayer;
    }
}
