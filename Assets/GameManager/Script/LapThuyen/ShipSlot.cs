﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShipSlot : MonoBehaviour
{
    [SerializeField] public List<LapThuyen> slot;
    [SerializeField] public Transform[] posSlot;

    public int indexMove; // Xác định phần tử bị kéo đi

    // Start is called before the first frame update
    void Start()
    {
        SetPos();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MoveToEmptySlot()
    {
        if (indexMove + 1 < slot.Count)
        {
            for (int i = indexMove + 1; i < slot.Count; i++)
            {
                slot[i].transform.DOMove(posSlot[i - 1].position, 0.5f);
            }
        }
        slot.RemoveAt(indexMove); // Xóa phần tử vừa bị kéo đi
        for (int i = 0; i < slot.Count; i++)
        {
            slot[i].id = i; //Gán lại ID cho các phần tử còn lại
        }
    }

    private void SetPos()
    {
        for (int i = 0; i < posSlot.Length; i++)
        {
            slot[i].transform.position = posSlot[i].position;
        }
    }
}
