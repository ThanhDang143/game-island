﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HinLapThuyen : MonoBehaviour
{
    public static HinLapThuyen ins;
    public GameObject startPoint;
    public GameObject[] endPoint;
    public GameObject[] endPointX;
    public GameObject hand;
    private Animator anim;
    public AnimationClip tay;
    public AnimationClip tayx;
    public static float waitHint = 0;

    void Start()
    {
        ins = this;
        waitHint = 0;
        anim = hand.GetComponent<Animator>();
        hand.SetActive(false);
        ControlGame.ins.hideHint = () => hand.SetActive(false);
    }

    void Update()
    {
        waitHint += Time.deltaTime;

        if (waitHint > 7f)
        {
            StartCoroutine(IEDragHint());
            waitHint = 2f;
        }
    }

    public IEnumerator IEDragHint()
    {
        for (int i = 0; i < endPoint.Length; i++)
        {
            if (startPoint.transform.position == endPointX[i].transform.position)
            {
                hand.transform.position = startPoint.transform.position - new Vector3(-0.35f, 0.75f, 0f);
                hand.SetActive(true);
                anim.Play(tay.name);
                hand.GetComponent<SpriteRenderer>().DOFade(1, 0.35f);
                yield return new WaitForSeconds(0.75f);
                hand.transform.DOMove(endPoint[i].transform.position - new Vector3(-0.35f, 0.75f, 0f), 2f);
                yield return new WaitForSeconds(2.5f);
                anim.Play(tayx.name);
                yield return new WaitForSeconds(0.75f);
                hand.GetComponent<SpriteRenderer>().DOFade(0, 0.35f);
                yield return new WaitForSeconds(0.35f);
                hand.SetActive(false);
            }
        }
    }

    public IEnumerator IETapHint(GameObject tapObj)
    {
        hand.transform.position = tapObj.transform.position - new Vector3(-0.35f, 0.75f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.25f);
        yield return new WaitForSeconds(1f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        hand.SetActive(false);
    }

    public IEnumerator IESwipHint(GameObject startSwip, GameObject endSwip)
    {
        hand.transform.position = startSwip.transform.position - new Vector3(-0.35f, 0.75f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.35f);
        yield return new WaitForSeconds(0.75f);
        hand.transform.DOMove(endSwip.transform.position - new Vector3(-0.35f, 0.75f, 0f), 1f);
        yield return new WaitForSeconds(1.5f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.35f);
        yield return new WaitForSeconds(0.35f);
        hand.SetActive(false);
    }
}
