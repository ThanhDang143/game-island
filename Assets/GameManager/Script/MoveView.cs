﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveView : MonoBehaviour
{
    void Update()
    {
        if (gameObject.CompareTag("Song"))
        {
            transform.position -= new Vector3(Time.deltaTime / 1.5f, 0f, 0f);
        }
        else if (gameObject.CompareTag("May"))
        {
            transform.position -= new Vector3(Time.deltaTime / 3f, 0f, 0f);
        }
    }
}
