﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockLevel : MonoBehaviour
{
    [SerializeField] GameObject watchVideo;
    Text text;

    private void Awake()
    {
        CheckLevel();
    }

    private void Start()
    {
        watchVideo.SetActive(false);
    }
    public void BtnUnlock()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        if (ControlCoin.ins.coin >= 50)
        {
            PlayerPrefs.SetInt(name, 1);
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
            GetComponent<Image>().color = Color.white;
            GetComponent<Button>().enabled = true;
            ControlCoin.ins.UpCoin(-50);

            StartCoroutine(ControlCoin.ins.IEShowCoin());
        }
        else
        {
            watchVideo.SetActive(true);
        }
    }

    public void BtnNo()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        watchVideo.SetActive(false);
    }

    public void BtnYes()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        ManagerAds.Ins.ShowRewardedVideo((s) =>
        {
            if (s)
            {
                ControlCoin.ins.UpCoin(5);
                StartCoroutine(ControlCoin.ins.IEShowCoin());
                watchVideo.SetActive(false);
            }
        });
    }

    private void CheckLevel()
    {
        PlayerPrefs.GetInt(name, 0);
        if (PlayerPrefs.GetInt(name) == 1)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
            GetComponent<Image>().color = Color.white;
            GetComponent<Button>().enabled = true;
            return;
        }
        if (PlayerPrefs.GetInt(name) == 0)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(true);
            GetComponent<Image>().color = Color.gray;
            GetComponent<Button>().enabled = false;
            return;
        }
    }
}
