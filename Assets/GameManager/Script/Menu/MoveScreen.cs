﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class MoveScreen : MonoBehaviour
{
    GameObject btnCoin;
    public static MoveScreen ins;
    public string selectedLevel;
    public GameObject screen;
    public GameObject logo;
    GameObject solidBlack;
    public GameObject btnPlay;
    public Sprite[] islandInScreen;
    public GameObject btnSound;
    public Sprite[] soundsSprite;
    public GameObject btnMusic;
    public Sprite[] musicsSprite;
    public RectTransform[] frame;
    public RectTransform rect;

    private void Start()
    {
        ins = this;
        btnCoin = ControlGame.ins.btnCoin;
        btnCoin.SetActive(false);
        solidBlack = GameObject.Find("SolidBlack");
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0f, 0.75f);

        StartCoroutine(IEZoomLoop(logo));
        StartCoroutine(IEZoomLoop(btnPlay));

        ControlGame.ins.btnCoin.SetActive(false);
        ControlGame.ins.btnPause.SetActive(false);
        ControlGame.ins.pauseMenu.SetActive(false);

        CheckSound();

        StartCoroutine(IEMoveFrame());
    }

    IEnumerator IEMoveFrame()
    {
        //Set initial
        btnPlay.GetComponent<Button>().enabled = false;
        frame[0].anchoredPosition = new Vector2(-300, 30);
        frame[1].anchoredPosition = new Vector2(300, 0);
        frame[2].anchoredPosition = new Vector2(0, 900);
        frame[3].anchoredPosition = new Vector2(0, -350);
        // Move
        frame[0].DOAnchorPosX(30, 0.5f);
        frame[1].DOAnchorPosX(-30, 0.5f);
        frame[2].DOAnchorPosY(350, 0.5f);
        frame[3].DOAnchorPosY(80, 0.5f);
        yield return new WaitForSeconds(0.5f);
        btnPlay.GetComponent<Button>().enabled = true;
    }

    IEnumerator IEZoomLoop(GameObject obj)
    {
        yield return new WaitForSeconds(0.5f);
        while (true)
        {
            obj.transform.DOScale(Vector3.one, 2f);
            yield return new WaitForSeconds(2f);
            obj.transform.DOScale(new Vector3(0.95f, 0.95f, 0.95f), 2f);
            yield return new WaitForSeconds(2f);
        }
    }

    public void MoveScreenS(int indexMove)
    {
        GetComponent<RectTransform>().DOAnchorPosY(rect.rect.height * indexMove, 0.5f);
    }

    public void BtnPlay()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        btnCoin.SetActive(true);
        MoveScreenS(1);
    }

    public void BtnBack1()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        btnCoin.SetActive(false);
        StartCoroutine(IEMoveFrame());
        MoveScreenS(0);
    }

    public void BtnBack2()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        MoveScreenS(1);
    }

    public void BtnSelectIsland(GameObject btn)
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        MoveScreenS(2);
        selectedLevel = btn.name;
        screen.GetComponent<Image>().sprite = islandInScreen[int.Parse(selectedLevel.Replace("btnBuilding", string.Empty)) - 1];
        screen.GetComponent<Image>().SetNativeSize();
        if (!PlayerPrefs.HasKey("Step" + selectedLevel))
        {
            PlayerPrefs.SetInt("Step" + selectedLevel, 0);
        }
        if (PlayerPrefs.GetInt("Step" + selectedLevel) >= 10)
        {
            SelectLevel.ins.scroll_pos = (1f / (SelectLevel.ins.pos.Length - 1f)) * 9;
        }
        else
        {
            SelectLevel.ins.scroll_pos = (1f / (SelectLevel.ins.pos.Length - 1f)) * PlayerPrefs.GetInt("Step" + selectedLevel);
        }
    }

    public void BtnRate()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        ManagerAds.Ins.RateApp();
    }

    public void BtnSound()
    {
        if (PlayerPrefs.GetInt(KeySaveGame.soundsEffectStatus) == 1)
        {
            btnSound.GetComponent<Image>().sprite = soundsSprite[0];
            PlayerPrefs.SetInt(KeySaveGame.soundsEffectStatus, 0);
            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (!FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 0;
                }
            }
        }
        else if (PlayerPrefs.GetInt(KeySaveGame.soundsEffectStatus) == 0)
        {
            btnSound.GetComponent<Image>().sprite = soundsSprite[1];
            PlayerPrefs.SetInt(KeySaveGame.soundsEffectStatus, 1);
            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (!FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 1;
                }
            }
        }
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
    }

    public void BtnMusic()
    {
        if (PlayerPrefs.GetInt(KeySaveGame.musicsStatus) == 1)
        {
            btnMusic.GetComponent<Image>().sprite = musicsSprite[0];
            PlayerPrefs.SetInt(KeySaveGame.musicsStatus, 0);

            for (int i = 0; i < FindObjectOfType<AudioManager>().transform.childCount; i++)
            {
                if (FindObjectOfType<AudioManager>().transform.GetChild(i).CompareTag("IsMusic"))
                {
                    FindObjectOfType<AudioManager>().transform.GetChild(i).GetComponent<AudioSource>().volume = 0;
                }
            }

            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 0;
                }
            }
        }
        else if (PlayerPrefs.GetInt(KeySaveGame.musicsStatus) == 0)
        {
            btnMusic.GetComponent<Image>().sprite = musicsSprite[1];
            PlayerPrefs.SetInt(KeySaveGame.musicsStatus, 1);

            for (int i = 0; i < FindObjectOfType<AudioManager>().transform.childCount; i++)
            {
                if (FindObjectOfType<AudioManager>().transform.GetChild(i).CompareTag("IsMusic"))
                {
                    FindObjectOfType<AudioManager>().transform.GetChild(i).GetComponent<AudioSource>().volume = 0.5f;
                }
            }

            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 0.5f;
                }
            }
        }
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
    }

    public void CheckSound()
    {
        if (PlayerPrefs.GetInt(KeySaveGame.musicsStatus) == 1)
        {
            btnMusic.GetComponent<Image>().sprite = musicsSprite[1];
            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 0.5f;
                }
            }
        }
        else if (PlayerPrefs.GetInt(KeySaveGame.musicsStatus) == 0)
        {
            btnMusic.GetComponent<Image>().sprite = musicsSprite[0];
            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 0;
                }
            }
        }

        if (PlayerPrefs.GetInt(KeySaveGame.soundsEffectStatus) == 1)
        {
            btnSound.GetComponent<Image>().sprite = soundsSprite[1];
            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (!FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 1;
                }
            }
        }
        else if (PlayerPrefs.GetInt(KeySaveGame.soundsEffectStatus) == 0)
        {
            btnSound.GetComponent<Image>().sprite = soundsSprite[0];
            for (int i = 0; i < FindObjectOfType<AudioManager>().sounds.Length; i++)
            {
                if (!FindObjectOfType<AudioManager>().sounds[i].isMusic)
                {
                    FindObjectOfType<AudioManager>().sounds[i].audioVolume = 0;
                }
            }
        }
    }
}
