﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BtnChooseStep : MonoBehaviour
{
    public int idThuyen;
    public int idStep;
    public void LoadScene()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        ControlGame.ins.idStep = idStep;
        ControlGame.ins.idThuyen = idThuyen;
        SceneManager.LoadScene("LapThuyen");
    }
}
