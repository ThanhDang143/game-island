﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SelectLevel : MonoBehaviour
{
    public static SelectLevel ins;
    public GameObject scrollbar;
    public float scroll_pos = 0;
    public float[] pos;
    [SerializeField] Sprite[] statusStep;

    void Start()
    {
        ins = this;
        // GetComponent<HorizontalLayoutGroup>().padding.left = Screen.width / 2 - 172;
        // GetComponent<HorizontalLayoutGroup>().padding.right = Screen.width / 2 - 172;
    }

    void Update()
    {
        pos = new float[transform.childCount];
        float distance = 1f / (pos.Length - 1f);

        for (int i = 0; i < pos.Length; i++)
        {
            pos[i] = distance * i;
        }

        if (Input.GetMouseButton(0))
        {
            scroll_pos = scrollbar.GetComponent<Scrollbar>().value;
        }
        else
        {
            for (int i = 0; i < pos.Length; i++)
            {
                if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
                {
                    scrollbar.GetComponent<Scrollbar>().value = Mathf.Lerp(scrollbar.GetComponent<Scrollbar>().value, pos[i], 0.1f);
                }
            }
        }

        for (int i = 0; i < pos.Length; i++)
        {
            if (scroll_pos < pos[i] + (distance / 2) && scroll_pos > pos[i] - (distance / 2))
            {
                if (i <= PlayerPrefs.GetInt("Step" + MoveScreen.ins.selectedLevel, 0))
                {
                    transform.GetChild(i).DOScale(new Vector3(1.2f, 1.2f, 1f), 0.5f);
                    transform.GetChild(i).gameObject.GetComponent<Button>().enabled = true;
                    transform.GetChild(i).GetComponent<Image>().DOColor(Color.white, 0.5f);
                    transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = statusStep[1];
                    transform.GetChild(i).GetChild(0).GetComponent<Image>().SetNativeSize();
                    AnimZoom(transform.GetChild(i).GetChild(0).gameObject);
                }
                else
                {
                    transform.GetChild(i).DOScale(new Vector3(1.2f, 1.2f, 1f), 0.5f);
                    transform.GetChild(i).gameObject.GetComponent<Button>().enabled = false;
                    transform.GetChild(i).GetComponent<Image>().DOColor(Color.gray, 0.5f);
                    transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                    transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = statusStep[0];
                    transform.GetChild(i).GetChild(0).GetComponent<Image>().SetNativeSize();
                    AnimZoom(transform.GetChild(i).GetChild(0).gameObject);
                }
                for (int j = 0; j < pos.Length; j++)
                {
                    if (j != i)
                    {
                        if (j <= PlayerPrefs.GetInt("Step" + MoveScreen.ins.selectedLevel, 0))
                        {
                            transform.GetChild(j).DOScale(new Vector3(0.8f, 0.8f, 1f), 0.5f);
                            transform.GetChild(j).gameObject.GetComponent<Button>().enabled = false;
                            transform.GetChild(j).GetComponent<Image>().DOColor(Color.white, 0.5f);
                            transform.GetChild(j).GetChild(0).gameObject.SetActive(false);
                        }
                        else
                        {
                            transform.GetChild(j).DOScale(new Vector3(0.8f, 0.8f, 1f), 0.5f);
                            transform.GetChild(j).gameObject.GetComponent<Button>().enabled = false;
                            transform.GetChild(j).GetComponent<Image>().DOColor(Color.grey, 0.5f);
                            transform.GetChild(j).GetChild(0).gameObject.SetActive(false);
                        }
                    }
                }
            }
        }
    }
    private void AnimZoom(GameObject imgBtn)
    {
        if (imgBtn.transform.localScale.x >= 1.02f)
        {
            imgBtn.transform.DOScale(new Vector3(0.98f, 0.98f, 1f), 1f);
            return;
        }
        if (imgBtn.transform.localScale.x <= 0.98f)
        {
            imgBtn.transform.DOScale(new Vector3(1.02f, 1.02f, 1f), 1f);
            return;
        }
    }

}

