﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class MoveBtnInMain : MonoBehaviour
{
    [SerializeField] Button btnRight;
    [SerializeField] Image imgRight;
    [SerializeField] Button btnLeft;
    [SerializeField] Image imgLeft;
    [SerializeField] GameObject[] btnBuilding;
    // Start is called before the first frame update
    void Start()
    {
        btnRight.enabled = true;
        imgRight.enabled = true;
        btnLeft.enabled = false;
        imgLeft.enabled = false;
    }

    public void BtnMoveRight()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        transform.DOMove(transform.position - new Vector3(15f, 0, 0), 0.5f);
        btnRight.enabled = false;
        imgRight.enabled = false;
        btnLeft.enabled = true;
        imgLeft.enabled = true;
    }

    public void BtnMoveLeft()
    {
        FindObjectOfType<AudioManager>().PlaySound("BtnClick", false);
        transform.DOMove(transform.position - new Vector3(-15f, 0, 0), 0.5f);
        btnRight.enabled = true;
        imgRight.enabled = true;
        btnLeft.enabled = false;
        imgLeft.enabled = false;
    }
}
