﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveMay : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(IEMove());
    }
    IEnumerator IEMove()
    {
        while (true)
        {
            float t = Random.Range(30, 60);
            GetComponent<RectTransform>().DOAnchorPosX(-1000, t).SetEase(Ease.Linear);
            yield return new WaitForSeconds(t);
            GetComponent<RectTransform>().anchoredPosition = new Vector2(1200, Random.Range(-280, 35));
        }
    }
}
