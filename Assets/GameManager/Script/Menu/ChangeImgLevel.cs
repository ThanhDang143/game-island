﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImgLevel : MonoBehaviour
{
    public Sprite img;

    void Start()
    {
        if (PlayerPrefs.GetInt("Step" + name) >= 10)
        {
            GetComponent<Image>().sprite = img;
            GetComponent<Image>().SetNativeSize();
        }
    }
}
