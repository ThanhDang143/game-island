﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
public class CocoAnim : MonoBehaviour
{
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;

    void Start()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
    }

}
