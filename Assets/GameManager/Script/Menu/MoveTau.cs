﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using DG.Tweening;

public class MoveTau : MonoBehaviour
{
    public Vector2 startPos;
    public Vector2 endPos;
    float time;

    void Start()
    {
        StartCoroutine(IEMove());
    }

    public IEnumerator IEMove()
    {
        while (true)
        {
            time = Random.Range(25f, 45f);
            GetComponent<RectTransform>().DOAnchorPos(endPos, time).SetEase(Ease.Linear);
            yield return new WaitForSeconds(time);
            GetComponent<RectTransform>().DOScaleX(GetComponent<RectTransform>().localScale.x * -1, 0);
            GetComponent<RectTransform>().DOAnchorPos(startPos, time).SetEase(Ease.Linear);
            yield return new WaitForSeconds(time);
            GetComponent<RectTransform>().DOScaleX(GetComponent<RectTransform>().localScale.x * -1, 0);
        }
    }
}
