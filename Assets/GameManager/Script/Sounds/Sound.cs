﻿using UnityEngine.Audio;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Sound
{
    public string audioName;
    public AudioClip audioClip;
    [Range(0f, 1f)] public float audioVolume = 1f;
    public bool isMusic;
}
