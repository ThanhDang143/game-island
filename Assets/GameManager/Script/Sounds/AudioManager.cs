﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public AudioSource[] audioThreads;
    [SerializeField] private GameObject autoAudioThead;

    void Awake()
    {

    }
    void Start()
    {
        PlaySound("Theme", true);
        StartCoroutine(IELoop());
    }

    public void PlaySound(string name, bool loop)
    {
        Sound s = Array.Find(sounds, sound => sound.audioName == name);
        if (s == null) Debug.Log("Not found " + name);
        for (int i = 0; i < audioThreads.Length; i++)
        {
            if (!audioThreads[i].isPlaying)
            {
                if (s.isMusic)
                {
                    audioThreads[i].tag = "IsMusic";
                }
                else
                {
                    audioThreads[i].tag = "IsSoundEffect";
                }
                audioThreads[i].clip = s.audioClip;
                audioThreads[i].volume = s.audioVolume;
                audioThreads[i].loop = loop;
                audioThreads[i].Play();
                return;
            }
            else if (i == audioThreads.Length - 1)
            {
                GameObject autoATh = Instantiate(autoAudioThead, transform);
                AudioSource asAutoATh = autoATh.GetComponent<AudioSource>();
                asAutoATh.clip = s.audioClip;
                asAutoATh.volume = s.audioVolume;
                asAutoATh.loop = loop;
                if (!loop) Destroy(autoATh, asAutoATh.clip.length);
                return;
            }
        }
    }

    public IEnumerator IEStopSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.audioName == name);
        if (s == null) Debug.Log("Not found " + name);
        for (int i = 0; i < audioThreads.Length; i++)
        {
            if (audioThreads[i].clip == s.audioClip)
            {
                while (audioThreads[i].volume > 0)
                {
                    audioThreads[i].volume -= 0.1f;
                    yield return new WaitForSeconds(0.1f);
                }
                audioThreads[i].Stop();
            }
        }
    }

    IEnumerator IELoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(15f, 20f));
            PlaySound("CoiTau", false);
        }
    }

}
