﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Spine;
using Spine.Unity;
public class Bom : MonoBehaviour
{
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string two;
    [SpineAnimation()] public string mix;
    [SerializeField] TextMeshProUGUI textXang;
    public int intXang = 0;

    void Start()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        GetComponent<BoxCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (intXang >= 100)
        {
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    private void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            FindObjectOfType<AudioManager>().PlaySound("BomXang", false);
            HintDoXang.waitHint = 0f;
            HintDoXang.ins.hand.SetActive(false);
            StartCoroutine(IEMoveBom());
        }
    }


    IEnumerator IEMoveBom()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, two, true);
        skeletonAnimation.AnimationState.SetAnimation(1, mix, true);
        for (int i = 0; i < 25; i++)
        {

            intXang += 1;
            if (intXang < 10)
            {
                textXang.text = "00" + intXang.ToString();
            }
            else if (intXang < 100)
            {
                textXang.text = "0" + intXang.ToString();
            }
            else
            {
                textXang.text = intXang.ToString();
                GetComponent<BoxCollider2D>().enabled = false;
            }
            yield return new WaitForSeconds(0.04f);
        }
        GetComponent<BoxCollider2D>().enabled = true;
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        skeletonAnimation.AnimationState.ClearTrack(1);
    }
}
