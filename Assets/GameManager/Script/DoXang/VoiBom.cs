﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class VoiBom : MonoBehaviour
{
    [SerializeField] Transform[] objDiemDoXang;
    [SerializeField] GameObject[] mask;
    Transform diemDoXang;
    GameObject thisMask;
    [SerializeField] GameObject bom;
    [SerializeField] Transform voiBom;
    Vector3 initialPos;



    void Start()
    {
        diemDoXang = objDiemDoXang[ControlGame.ins.idThuyen];
        thisMask = mask[ControlGame.ins.idThuyen];
        thisMask.SetActive(false);
        StartCoroutine(IEStart());
    }


    void Update()
    {
        if (Mathf.Abs(transform.position.x - voiBom.transform.position.x) > 2f)
        {
            voiBom.transform.DOMoveX(transform.position.x, 1f);
        }
        if (bom.GetComponent<Bom>().intXang == 100)
        {
            StartCoroutine(IEWaitToMoveBack());
        }

    }

    private void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            FindObjectOfType<AudioManager>().PlaySound("Keo",false);
            if (ControlGame.OnceClick()) return;
            HintDoXang.waitHint = 0f;
            transform.DOMove(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, initialPos.z), 0.1f);
        }
    }

    private void OnMouseDrag()
    {
        if (Time.timeScale != 0)
        {
            if (ControlGame.OnceClick()) return;
            HintDoXang.waitHint = 0f;
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z);
        }
    }

    public void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            HintDoXang.waitHint = 0f;
            FindObjectOfType<AudioManager>().PlaySound("Tha",false);
            if (Mathf.Abs(transform.position.x - diemDoXang.position.x) <= 0.5f && Mathf.Abs(transform.position.y - diemDoXang.position.y) <= 0.5f)
            {
                StartCoroutine(IEMoveVoiBom());
                bom.GetComponent<BoxCollider2D>().enabled = true;
                GetComponent<BoxCollider2D>().enabled = false;

            }
            else
            {
                transform.DOMove(initialPos + new Vector3(2f, -4.5f, 0f), 0.75f);
            }
        }
    }

    IEnumerator IEMoveVoiBom()
    {
        transform.DOMoveY(diemDoXang.position.y + 0.5f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        ControlVoiBom.ins.skeletonAnimation.AnimationState.ClearTracks();
        thisMask.SetActive(true);
        transform.DOMove(diemDoXang.position - new Vector3(0.075f, 0.25f, 0f), 0.5f);
        transform.SetParent(thisMask.transform);
    }

    IEnumerator IEWaitToMoveBack()
    {
        bom.GetComponent<Bom>().intXang += 1;
        yield return new WaitForSeconds(1.5f);
        transform.DOMove(new Vector3(5f, 5f), 2.5f);
    }

    IEnumerator IEStart()
    {
        initialPos = new Vector3(2f, 4.5f, -1.0f);
        transform.position = initialPos + new Vector3(0f, 1f, 0f);
        voiBom.transform.position = initialPos;
        GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(4f);
        transform.DOMove(initialPos + new Vector3(2f, -4.5f, 0f), 2f);
    }
}
