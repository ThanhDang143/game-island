﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class ControlVoiBom : MonoBehaviour
{
    public static ControlVoiBom ins;
    [SerializeField] Transform target;
    public SkeletonAnimation skeletonAnimation;
    [SerializeField] [SpineBone()] private string boneTarget;
    [SerializeField] private Bone bone;
    [SpineAnimation()] public string idle;

    void Start()
    {
        ins = this;
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        bone = skeletonAnimation.Skeleton.FindBone(boneTarget);
        skeletonAnimation.UpdateLocal += SkeletonAnimation_UpdateLocal;
    }

    private void SkeletonAnimation_UpdateLocal(ISkeletonAnimation animated)
    {
        var localPosition = transform.InverseTransformPoint(target.position + new Vector3(-1f, -0.9f, 0f));
        bone.SetLocalPosition(localPosition);
    }
}
