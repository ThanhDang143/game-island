using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlDoXang : MonoBehaviour
{
    [SerializeField] GameObject[] ship;

    void Start()
    {
        for (int i = 0; i < ship.Length; i++)
        {
            ship[i].SetActive(ControlGame.ins.idThuyen == i);
        }
        ControlGame.ins.cam = Camera.main;
    }
}
