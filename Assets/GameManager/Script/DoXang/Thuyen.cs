﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Spine.Unity;
using Spine;
public class Thuyen : MonoBehaviour
{
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string load1;
    [SpineAnimation()] public string load2;
    [SpineAnimation()] public string load3;
    [SerializeField] GameObject bom;
    [SerializeField] GameObject voiBom;
    [SerializeField] GameObject effect;

    void Start()
    {
        StartCoroutine(IEStart());
    }

    // Update is called once per frame
    void Update()
    {
        if (bom.GetComponent<Bom>().intXang == 101)
        {
            StartCoroutine(IEWaitToMove());
        }
    }

    IEnumerator IEWaitToMove()
    {
        bom.GetComponent<Bom>().intXang += 1;
        yield return new WaitForSeconds(2f);
        effect.GetComponent<ParticleSystem>().Play();
        skeletonAnimation.AnimationState.SetAnimation(2, load3, false);
        effect.SetActive(true);
        yield return new WaitForSeconds(0.75f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        skeletonAnimation.AnimationState.SetAnimation(3, move, true);
        transform.DOMoveX(transform.position.x + 15f, 7.5f);
        yield return new WaitForSeconds(6.5f);
        SceneManager.LoadScene("Step");
    }

    IEnumerator IEStart()
    {
        effect.GetComponent<ParticleSystem>().Play();
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 10.8f, 6f);
        yield return new WaitForSeconds(6f);
        effect.GetComponent<ParticleSystem>().Stop();
        voiBom.GetComponent<BoxCollider2D>().enabled = true;
        skeletonAnimation.AnimationState.SetAnimation(2, load1, false);
        yield return new WaitForSeconds(0.167f);
        skeletonAnimation.AnimationState.SetAnimation(2, load2, true);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }
}
