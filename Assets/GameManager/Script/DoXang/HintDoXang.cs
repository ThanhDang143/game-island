﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HintDoXang : MonoBehaviour
{
    public static HintDoXang ins;
    public GameObject voiBom;
    public GameObject bom;
    public GameObject diemDoXang;
    public GameObject hand;
    private Animator anim;
    public AnimationClip tay;
    public AnimationClip tayx;
    public static float waitHint = 0;

    void Start()
    {
        ins = this;
        waitHint = 0;
        anim = hand.GetComponent<Animator>();
        ControlGame.ins.hideHint = () => hand.SetActive(false);
        hand.SetActive(false);
    }

    void Update()
    {
        waitHint += Time.deltaTime;

        if (waitHint > 7f && voiBom.GetComponent<BoxCollider2D>().isActiveAndEnabled)
        {
            StartCoroutine(IESwipHint(voiBom, diemDoXang));
            waitHint = 2f;
        }

        if (waitHint > 7f && !voiBom.GetComponent<BoxCollider2D>().isActiveAndEnabled && bom.GetComponent<BoxCollider2D>().isActiveAndEnabled)
        {
            StartCoroutine(IETapHint(bom));
            waitHint = 2f;
        }

    }

    public IEnumerator IETapHint(GameObject tapObj)
    {
        hand.transform.position = tapObj.transform.position - new Vector3(-0.35f, 0.75f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.25f);
        yield return new WaitForSeconds(1f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        hand.SetActive(false);
    }

    public IEnumerator IESwipHint(GameObject startSwip, GameObject endSwip)
    {
        hand.transform.position = startSwip.transform.position - new Vector3(-0.35f, 0.75f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.35f);
        yield return new WaitForSeconds(0.75f);
        hand.transform.DOMove(endSwip.transform.position - new Vector3(0.35f, 1.5f, 0f), 1f);
        yield return new WaitForSeconds(1.5f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.35f);
        yield return new WaitForSeconds(0.35f);
        hand.SetActive(false);
    }
}
