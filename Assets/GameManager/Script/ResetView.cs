﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class ResetView : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("May"))
        {
            other.transform.position = new Vector3(25f, Random.Range(2f, 3f), other.transform.position.z);
        }
        else if (other.CompareTag("Song"))
        {
            other.transform.position = new Vector3(25f, other.transform.position.y, other.transform.position.z);
        }
    }
}
