﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MongStep4 : MonoBehaviour
{
    [SerializeField] GameObject da;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("May") || other.CompareTag("Song")) return;
        other.gameObject.transform.SetParent(transform);
        da.transform.position += new Vector3(0f, 0.035f, 0f);
    }
}
