﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreSong : MonoBehaviour
{
    [SerializeField] GameObject[] song;
    [SerializeField] GameObject[] may;
    void Start()
    {
        StartCoroutine(IEStart());
    }
    IEnumerator IEStart()
    {
        yield return new WaitForSeconds(1.5f);

        for (int i = 0; i < transform.childCount; i++)
        {
            for (int s = 0; s < song.Length; s++)
            {
                Physics2D.IgnoreCollision(transform.GetChild(i).GetComponent<Collider2D>(), song[s].GetComponent<Collider2D>());
            }
            for (int m = 0; m < may.Length; m++)
            {
                Physics2D.IgnoreCollision(transform.GetChild(i).GetComponent<Collider2D>(), may[m].GetComponent<Collider2D>());
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
