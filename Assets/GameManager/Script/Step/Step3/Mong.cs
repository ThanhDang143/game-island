﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Mong : MonoBehaviour
{
    [SerializeField] Transform[] day;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Rac") || other.CompareTag("May") || other.CompareTag("Song")) return;
        for (int i = 0; i < day.Length; i++)
        {
            day[i].GetComponent<SpriteRenderer>().DOFade(0, 0.25f);
        }
        other.gameObject.transform.SetParent(transform);
        other.gameObject.transform.DOMoveY(other.gameObject.transform.position.y - 0.65f, 1f);
    }
}
