﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Spine.Unity;
using Spine;

public class Tau2Step3 : MonoBehaviour
{
    public static Tau2Step3 ins;
    public SkeletonAnimation skeletonAnimation;
    [SerializeField] [SpineBone()] private string boneTarget;
    [SerializeField] private Bone bone;
    [SerializeField] GameObject khungNang;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string move1;
    [SpineAnimation()] public string move2;
    [SpineAnimation()] public string stop1;
    [SpineAnimation()] public string stop2;
    [SpineAnimation()] public string pull1;
    [SpineAnimation()] public string pull2;
    [SpineAnimation()] public string pull3;
    [SpineAnimation()] public string pull2Target;
    [SerializeField] GameObject number;
    [SerializeField] GameObject solidBlack;
    [SerializeField] Sprite[] numberSprite;
    [SerializeField] GameObject[] effect;
    [SerializeField] Transform[] day;
    float startSwipe;
    float endSwipe;

    public int tap = 1;
    // Start is called before the first frame update
    void Start()
    {
        ins = this;
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < effect.Length; i++)
        {
            effect[i].SetActive(false);
        }

        for (int i = 0; i < day.Length; i++)
        {
            day[i].GetComponent<SpriteRenderer>().DOFade(0, 0);
        }
        StartCoroutine(IEStart());
        bone = skeletonAnimation.Skeleton.FindBone(boneTarget);
        skeletonAnimation.UpdateLocal += SkeletonAnimation_UpdateLocal;
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            HintStep3.waitHint = 0f;
            endSwipe = Input.mousePosition.y;

            if (tap == 1)
            {
                StartCoroutine(IETap1());
                return;
            }

            if (tap == 2 && CheckSwipe())
            {
                StartCoroutine(IESwipe1());
                return;
            }

            if (tap == 3)
            {
                StartCoroutine(IETap2());
                return;
            }

            if (tap == 4 && CheckSwipe())
            {
                StartCoroutine(IESwipe2());
                return;
            }

            if (tap == 5)
            {
                StartCoroutine(IETap3());
                return;
            }

            if (tap == 6 && CheckSwipe())
            {
                StartCoroutine(IESwipe3());
                return;
            }
        }
    }

    private void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            HintStep3.waitHint = 0f;
            HintStep3.ins.hand.SetActive(false);
            startSwipe = Input.mousePosition.y;
        }
    }

    private bool CheckSwipe()
    {
        if (startSwipe - endSwipe > 0) return true;
        return false;
    }

    IEnumerator IESpawnNumber(Vector3 spawnPos)
    {
        GameObject n = Instantiate(number, spawnPos, Quaternion.identity);
        n.GetComponent<SpriteRenderer>().sprite = numberSprite[tap / 2 - 1];
        n.transform.DOScale(new Vector3(2f, 2f, 0f), 1f);
        yield return new WaitForSeconds(1.5f);
        n.GetComponent<SpriteRenderer>().DOFade(0f, 1);
        yield return new WaitForSeconds(1f);
        Destroy(n);
    }

    private void SkeletonAnimation_UpdateLocal(ISkeletonAnimation animated)
    {
        var localPosition = transform.InverseTransformPoint(khungNang.transform.position);
        bone.SetLocalPosition(localPosition);
    }

    IEnumerator IEStart()
    {
        effect[0].SetActive(true);
        effect[1].SetActive(true);
        khungNang.transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, move1, true);
        transform.DOMoveX(transform.position.x + 11f, 10f);
        yield return new WaitForSeconds(9f);
        skeletonAnimation.AnimationState.SetAnimation(1, pull1, false).TimeScale = 0.5f;
        effect[1].GetComponent<ParticleSystem>().Stop();
        yield return new WaitForSeconds(0.667f * 2f);
        effect[2].SetActive(true);
        effect[3].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(2, move2, true);
        skeletonAnimation.AnimationState.ClearTrack(0);
        yield return new WaitForSeconds(2f);
        HintStep3.waitHint = 0f;
        khungNang.transform.GetChild(0).gameObject.SetActive(true);
        GetComponent<BoxCollider2D>().enabled = true;
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    IEnumerator IETap1()
    {
        FindObjectOfType<AudioManager>().PlaySound("LayKhoiBeTong", true);
        GetComponent<BoxCollider2D>().enabled = false;
        //  Hạ khung xuống 
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).Delay = 1f;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).AnimationStart = 1f;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).TimeScale = 0f;
        khungNang.transform.DOMoveY(khungNang.transform.position.y - 0.35f, 1f);
        yield return new WaitForSeconds(1f);
        //  Đưa khung lên
        for (int i = 0; i < day.Length; i++)
        {
            day[i].GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
        }
        khungNang.transform.DOMoveY(khungNang.transform.position.y + 0.35f, 1f);
        yield return new WaitForSeconds(1.5f);
        //  Tàu chạy đến bị trí thả
        Tau7Step3.ins.CheckEnd();
        transform.DOMoveX(transform.position.x + 1f, 2f);
        yield return new WaitForSeconds(2.25f);
        GetComponent<BoxCollider2D>().enabled = true;
        tap++;
        HintStep3.waitHint = 0f;
    }

    IEnumerator IESwipe1()
    {
        if (CheckSwipe())
        {
            StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("LayKhoiBeTong"));
            FindObjectOfType<AudioManager>().PlaySound("ThaKhoiBeTong", true);
            GetComponent<BoxCollider2D>().enabled = false;
            //  Thả móng xuống
            khungNang.transform.DOMoveY(khungNang.transform.position.y - 3f, 3f);
            yield return new WaitForSeconds(2.5f);
            StartCoroutine(IESpawnNumber(new Vector3(3f, -2f, -3f)));
            yield return new WaitForSeconds(0.5f);
            // Kéo khung lên
            khungNang.transform.DOMoveY(khungNang.transform.position.y + 3f, 3f);
            yield return new WaitForSeconds(3.25f);
            StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("ThaKhoiBeTong"));
            // // Tàu về vị trí cũ
            transform.DOMoveX(transform.position.x - 1f, 2f);
            yield return new WaitForSeconds(1.8f);
            GetComponent<BoxCollider2D>().enabled = true;
            skeletonAnimation.AnimationState.SetAnimation(3, move2, true);
            tap++;
            HintStep3.waitHint = 0f;
        }
    }

    IEnumerator IETap2()
    {
        FindObjectOfType<AudioManager>().PlaySound("LayKhoiBeTong", true);
        GetComponent<BoxCollider2D>().enabled = false;
        //  Hạ khung xuống 
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).Delay = 1f;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).AnimationStart = 1f;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).TimeScale = 0f;
        khungNang.transform.GetChild(0).gameObject.GetComponent<BoxCollider2D>().enabled = true;
        khungNang.transform.DOMoveY(khungNang.transform.position.y - 0.45f, 1f);
        yield return new WaitForSeconds(1.5f);
        //  Đưa khung lên
        for (int i = 0; i < day.Length; i++)
        {
            day[i].GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
        }
        khungNang.transform.DOMoveY(khungNang.transform.position.y + 0.45f, 1f);
        yield return new WaitForSeconds(1.5f);
        //  Tàu chạy đến bị trí thả
        Tau7Step3.ins.CheckEnd();
        transform.DOMoveX(transform.position.x - 0.5f, 1f);
        yield return new WaitForSeconds(1.25f);

        GetComponent<BoxCollider2D>().enabled = true;
        HintStep3.waitHint = 0f;
        tap++;
    }

    IEnumerator IESwipe2()
    {
        if (CheckSwipe())
        {
            StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("LayKhoiBeTong"));
            FindObjectOfType<AudioManager>().PlaySound("ThaKhoiBeTong", true);
            GetComponent<BoxCollider2D>().enabled = false;
            //  Thả móng xuống
            khungNang.transform.DOMoveY(khungNang.transform.position.y - 3f, 3f);
            yield return new WaitForSeconds(2.5f);
            StartCoroutine(IESpawnNumber(new Vector3(3f, -2f, -3f)));
            yield return new WaitForSeconds(0.5f);
            // Kéo khung lên
            khungNang.transform.DOMoveY(khungNang.transform.position.y + 3f, 3f);
            yield return new WaitForSeconds(3.25f);
            StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("ThaKhoiBeTong"));
            // Tàu về vị trí cũ
            transform.DOMoveX(transform.position.x + 0.5f, 1f);
            yield return new WaitForSeconds(1.25f);

            GetComponent<BoxCollider2D>().enabled = true;
            skeletonAnimation.AnimationState.SetAnimation(3, move2, true);
            HintStep3.waitHint = 0f;
            tap++;
        }
    }

    IEnumerator IETap3()
    {
        FindObjectOfType<AudioManager>().PlaySound("LayKhoiBeTong", true);
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).Delay = 1f;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).AnimationStart = 1f;
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, false).TimeScale = 0f;
        khungNang.transform.GetChild(0).gameObject.GetComponent<BoxCollider2D>().enabled = true;
        //  Hạ khung xuống 
        khungNang.transform.DOMoveY(khungNang.transform.position.y - 0.6f, 1f);
        yield return new WaitForSeconds(1.5f);
        //  Đưa khung lên
        for (int i = 0; i < day.Length; i++)
        {
            day[i].GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
        }
        khungNang.transform.DOMoveY(khungNang.transform.position.y + 0.6f, 1f);
        yield return new WaitForSeconds(1.5f);
        //  Tàu chạy đến bị trí thả
        Tau7Step3.ins.CheckEnd();
        transform.DOMoveX(transform.position.x - 1.5f, 2.5f);
        yield return new WaitForSeconds(2.75f);

        GetComponent<BoxCollider2D>().enabled = true;
        HintStep3.waitHint = 0f;
        tap++;
    }

    IEnumerator IESwipe3()
    {
        if (CheckSwipe())
        {
            StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("LayKhoiBeTong"));
            FindObjectOfType<AudioManager>().PlaySound("ThaKhoiBeTong", true);
            //  Thả móng xuống
            GetComponent<BoxCollider2D>().enabled = false;
            khungNang.transform.DOMoveY(khungNang.transform.position.y - 3f, 3f);
            yield return new WaitForSeconds(2.5f);
            StartCoroutine(IESpawnNumber(new Vector3(3f, -2f, -3f)));
            yield return new WaitForSeconds(0.5f);
            // Kéo khung lên
            khungNang.transform.DOMoveY(khungNang.transform.position.y + 3f, 3f);
            yield return new WaitForSeconds(3f);
            skeletonAnimation.AnimationState.SetAnimation(3, pull3, false).TimeScale = 0.5f;
            effect[2].GetComponent<ParticleSystem>().Stop();
            effect[3].GetComponent<ParticleSystem>().Stop();
            yield return new WaitForSeconds(0.667f * 2f);
            StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("ThaKhoiBeTong"));
            FindObjectOfType<AudioManager>().PlaySound("Move", true);
            skeletonAnimation.AnimationState.SetAnimation(4, move1, true);
            effect[1].GetComponent<ParticleSystem>().Play();
            transform.DOMoveX(transform.position.x + 14f, 10f);
            HintStep3.waitHint = 0f;
            yield return new WaitForSeconds(9f);
            solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("RuaThuyen");
        }
    }
}
