﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HintStep3 : MonoBehaviour
{
    public static HintStep3 ins;
    public GameObject hand;
    private Animator anim;
    public AnimationClip tay;
    public AnimationClip tayx;
    public static float waitHint;

    void Start()
    {
        ins = this;
        waitHint = 0f;
        anim = hand.GetComponent<Animator>();
        ControlGame.ins.hideHint = () => hand.SetActive(false);
        hand.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        waitHint += Time.deltaTime;

        if (waitHint > 10f && (Tau2Step3.ins.tap == 1f || Tau2Step3.ins.tap == 3f || Tau2Step3.ins.tap == 5f))
        {
            StartCoroutine(IETapHint(gameObject));
            waitHint = 5f;
        }

        if (waitHint > 10f && (Tau2Step3.ins.tap == 2f || Tau2Step3.ins.tap == 4f || Tau2Step3.ins.tap == 6f))
        {
            StartCoroutine(IESwipHint(gameObject));
            waitHint = 5f;
        }
    }

    public IEnumerator IETapHint(GameObject tapObj)
    {
        hand.SetActive(true);
        hand.transform.position = tapObj.transform.position - new Vector3(-0.5f, 0f, 0f);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.25f);
        yield return new WaitForSeconds(1f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        hand.SetActive(false);
    }
    public IEnumerator IESwipHint(GameObject swipPoint)
    {
        hand.transform.position = swipPoint.transform.position - new Vector3(-0.5f, -1f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.35f);
        yield return new WaitForSeconds(0.75f);
        hand.transform.DOMove(swipPoint.transform.position - new Vector3(-0.5f, 1f, 0f), 1f);
        yield return new WaitForSeconds(1.5f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.35f);
        yield return new WaitForSeconds(0.35f);
        hand.SetActive(false);
    }
}
