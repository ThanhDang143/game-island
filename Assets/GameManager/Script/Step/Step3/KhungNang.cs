﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class KhungNang : MonoBehaviour
{
    [SerializeField] GameObject mongPos;
    [SerializeField] float mongY;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("May") || other.CompareTag("Song")) return;
        GetComponent<BoxCollider2D>().enabled = false;
        StartCoroutine(IEOnTrigger(other));
    }

    IEnumerator IEOnTrigger(Collider2D other)
    {
        other.GetComponent<SpriteRenderer>().sortingOrder = 8;
        other.gameObject.transform.DOMove(mongPos.transform.position - new Vector3(0f, mongY, 0f), 0.5f);
        other.gameObject.transform.SetParent(transform);
        yield return new WaitForSeconds(0.5f);
        other.gameObject.transform.DOMove(mongPos.transform.position + new Vector3(0f, 0.2f, 0f), 0.75f);
    }
}
