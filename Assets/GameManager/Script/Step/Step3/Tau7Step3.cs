﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Spine.Unity;
using Spine;

public class Tau7Step3 : MonoBehaviour
{
    public static Tau7Step3 ins;
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string move;
    [SerializeField] int countMong;

    // Start is called before the first frame update
    void Start()
    {
        ins = this;
        countMong = transform.GetChild(1).childCount;
        StartCoroutine(IEStart());
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void CheckEnd()
    {
        countMong--;
        if (countMong <= 0)
        {
            StartCoroutine(IEEnd());
        }
    }

    IEnumerator IEStart()
    {
        yield return new WaitForSeconds(8f);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x - 7f, 5f);
        yield return new WaitForSeconds(5f);
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true);
    }

    IEnumerator IEEnd()
    {
        yield return new WaitForSeconds(3.5f);
        transform.DOMoveX(transform.position.x + 7f, 10f);
    }

}
