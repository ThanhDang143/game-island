﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStep : MonoBehaviour
{
    public virtual void OnShow()
    {
        gameObject.SetActive(true);
    }
}
