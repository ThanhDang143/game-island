﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TauCon : MonoBehaviour
{
    public bool tap2;
    [SerializeField] Transform tauConParent;
    [SerializeField] GameObject effect;
    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            tap2 = false;
            StartCoroutine(IETap2());
        }
    }

    IEnumerator IETap2()
    {
        effect.SetActive(false);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("TauConDiChuyen"));
        GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        GetComponent<SpriteRenderer>().flipX = true;
        transform.DOMoveX(transform.position.x - 6f, 6f);
        yield return new WaitForSeconds(6.5f);
        transform.SetParent(tauConParent);
        GetComponent<SpriteRenderer>().flipX = false;
        yield return new WaitForSeconds(0.5f);
        tap2 = true;
    }
}
