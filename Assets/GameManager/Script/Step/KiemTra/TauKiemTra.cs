﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Spine;
using Spine.Unity;
using UnityEngine.SceneManagement;

public class TauKiemTra : MonoBehaviour
{
    [SerializeField] GameObject tauCon;
    [SerializeField] GameObject solidBlack;
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SerializeField] private Bone bone;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string pull1;
    [SpineAnimation()] public string pull2;
    [SpineAnimation()] public string pull3;
    [SerializeField] GameObject[] effect;

    void Start()
    {
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < effect.Length; i++)
        {
            effect[i].SetActive(false);
        }
        StartCoroutine(IEStart());
    }

    void Update()
    {
        if (tauCon.GetComponent<TauCon>().tap2 == true)
        {
            tauCon.GetComponent<TauCon>().tap2 = false;
            StartCoroutine(IEEnd());
        }
    }

    IEnumerator IEStart()
    {
        effect[0].SetActive(true);
        effect[1].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        tauCon.GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOMoveX(-2.5f, 5f);
        yield return new WaitForSeconds(5f);
        effect[1].GetComponent<ParticleSystem>().Stop();
        effect[2].SetActive(true);
        effect[3].SetActive(true);
        GetComponent<BoxCollider2D>().enabled = true;
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            HintStepKiemTra.waitHint = -2f;
            StartCoroutine(IETap1());
        }
    }

    IEnumerator IETap1()
    {
        FindObjectOfType<AudioManager>().PlaySound("ThaTauKiemTra", true);
        skeletonAnimation.AnimationState.SetAnimation(1, pull1, false);
        effect[4].GetComponent<SpriteRenderer>().DOFade(0, Time.deltaTime);
        GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(2.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("ThaTauKiemTra"));
        effect[4].SetActive(true);
        effect[4].GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
        skeletonAnimation.AnimationState.SetAnimation(3, pull2, true);
        tauCon.transform.SetParent(transform);
        FindObjectOfType<AudioManager>().PlaySound("TauConDiChuyen", true);
        tauCon.transform.DOMoveX(tauCon.transform.position.x + 6f, 8f);
        yield return new WaitForSeconds(8.5f);
        tauCon.GetComponent<BoxCollider2D>().enabled = true;
    }

    IEnumerator IEEnd()
    {
        FindObjectOfType<AudioManager>().PlaySound("KeoTauConLen", true);
        skeletonAnimation.AnimationState.SetAnimation(3, pull3, false);
        yield return new WaitForSeconds(2f);
        effect[1].GetComponent<ParticleSystem>().Play();
        effect[2].GetComponent<ParticleSystem>().Stop();
        effect[3].GetComponent<ParticleSystem>().Stop();
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("KeoTauConLen"));
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        skeletonAnimation.AnimationState.SetAnimation(4, move, true);
        transform.DOMoveX(transform.position.x + 14f, 8f);
        yield return new WaitForSeconds(7f);
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("RuaThuyen");
    }
}
