﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class PlayAnimRongBien : MonoBehaviour
{
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;

    void Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true).TimeScale = Random.Range(0.25f, 0.75f);
    }
}
