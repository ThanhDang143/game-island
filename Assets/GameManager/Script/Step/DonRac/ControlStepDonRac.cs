﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlStepDonRac : BaseStep
{
    public GameObject rac;
    public Sprite[] racSprite;

    public override void OnShow()
    {
        base.OnShow();
        for (int i = 0; i <= 100; i++)
        {
            SpawnRac();
        }
    }

    private void SpawnRac()
    {
        GameObject n = Instantiate(rac, new Vector3(Random.Range(-10f, 10f), Random.Range(-0.3f, -3f), 0.0f), Quaternion.identity);
        n.GetComponent<SpriteRenderer>().sprite = racSprite[Random.Range(0, racSprite.Length)];
    }
}
