﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Spine;
using Spine.Unity;

public class ControlTau : MonoBehaviour
{
    int tap = 0;
    [SerializeField] GameObject disableTap;
    [SerializeField] GameObject[] effect;
    [SerializeField] GameObject[] view;
    [SerializeField] GameObject[] way;
    [SerializeField] GameObject solidBlack;
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string one;


    void Start()
    {
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < view.Length; i++)
        {
            view[i].SetActive(false);
        }
        for (int i = 0; i < effect.Length; i++)
        {
            effect[i].SetActive(false);
        }

        StartCoroutine(IEStart());
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            if (tap == 0)
            {
                StartCoroutine(IETap1());
                tap++;
                return;
            }

            if (tap == 1)
            {
                StartCoroutine(IETap2());
                return;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Rac"))
        {
            other.transform.SetParent(transform);
            other.GetComponent<BoxCollider2D>().enabled = false;
            StartCoroutine(IEDestroyRac(other.gameObject));
        }
    }

    IEnumerator IEStart()
    {
        disableTap.SetActive(true);
        effect[0].SetActive(true);
        effect[1].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 9, 6f);
        yield return new WaitForSeconds(6f);
        HintDonRac.waitHint = -5;
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        skeletonAnimation.AnimationState.SetAnimation(1, mix, true);
        effect[1].GetComponent<ParticleSystem>().Stop();
        effect[2].SetActive(true);
        effect[3].SetActive(true);
        disableTap.SetActive(false);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    IEnumerator IEDestroyRac(GameObject objRac)
    {
        objRac.transform.DOLocalMove(way[0].transform.localPosition, 0.25f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.25f);
        objRac.transform.DOLocalMove(way[1].transform.localPosition, 0.5f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.5f);
        objRac.transform.DOLocalMove(way[2].transform.localPosition, 0.25f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.25f);
        objRac.transform.DOLocalMove(way[3].transform.localPosition, 0.5f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.5f);
        Destroy(objRac);
    }

    IEnumerator IETap1()
    {
        disableTap.SetActive(true);
        FindObjectOfType<AudioManager>().PlaySound("DonRac", true);
        effect[1].GetComponent<ParticleSystem>().Play();
        effect[2].GetComponent<ParticleSystem>().Stop();
        effect[3].GetComponent<ParticleSystem>().Stop();
        skeletonAnimation.AnimationState.ClearTrack(1);
        skeletonAnimation.AnimationState.SetAnimation(0, one, true);
        HintDonRac.waitHint = -5;
        transform.DOMoveX(11.75f, 10f);
        yield return new WaitForSeconds(9f);
        transform.localScale = new Vector3(-0.7f, 0.7f, 1f);
        effect[1].transform.localScale = new Vector3(-0.75f, 0.75f, 1f);
        transform.DOMoveY(transform.position.y - 1.2f, 0f);
        yield return new WaitForSeconds(1f);
        transform.DOMoveX(4f, 4f);
        yield return new WaitForSeconds(4f);
        effect[1].GetComponent<ParticleSystem>().Stop();
        effect[2].GetComponent<ParticleSystem>().Play();
        effect[3].GetComponent<ParticleSystem>().Play();
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        skeletonAnimation.AnimationState.SetAnimation(1, mix, true);
        HintDonRac.waitHint = -5;
        disableTap.SetActive(false);
        yield return new WaitForSeconds(1f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("DonRac"));
    }

    IEnumerator IETap2()
    {
        FindObjectOfType<AudioManager>().PlaySound("DonRac", true);
        effect[1].GetComponent<ParticleSystem>().Play();
        effect[2].GetComponent<ParticleSystem>().Stop();
        effect[3].GetComponent<ParticleSystem>().Stop();
        disableTap.SetActive(true);
        skeletonAnimation.AnimationState.ClearTrack(1);
        skeletonAnimation.AnimationState.SetAnimation(0, one, true);
        HintDonRac.waitHint = -5;
        transform.DOMoveX(-12f, 7f);
        yield return new WaitForSeconds(6f);
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
        yield return new WaitForSeconds(1f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        SceneManager.LoadScene("RuaThuyen");
    }
}
