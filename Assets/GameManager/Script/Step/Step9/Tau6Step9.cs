﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;
using UnityEngine.SceneManagement;

public class Tau6Step9 : MonoBehaviour
{
    [SerializeField] GameObject[] view;
    [SerializeField] GameObject[] effect;
    [SerializeField] GameObject[] item;
    [SerializeField] GameObject[] itemEndPos;
    [SerializeField] Transform[] way;
    [SerializeField] Transform bone4;
    [SerializeField] Transform island;
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string action1;
    [SpineAnimation()] public string action2;
    [SpineAnimation()] public string action3;
    [SerializeField] GameObject solidBlack;
    int tap;

    void Start()
    {
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < view.Length; i++)
        {
            view[i].SetActive(false);
        }
        for (int i = 0; i < effect.Length; i++)
        {
            effect[i].SetActive(false);
        }
        for (int i = 0; i < item.Length; i++)
        {
            item[i].transform.position = way[0].transform.position;
            item[i].transform.localScale = new Vector3(0.6f, 0.6f, 1);
        }
        effect[0].SetActive(true);
        view[0].SetActive(true);
        tap = 1;
        GetComponent<BoxCollider2D>().enabled = false;
        StartCoroutine(IEStart());
    }

    IEnumerator IEStart()
    {
        effect[1].SetActive(true);
        effect[2].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 8f, 5.5f);
        yield return new WaitForSeconds(7f);
        GetComponent<BoxCollider2D>().enabled = true;
        effect[2].GetComponent<ParticleSystem>().Stop();
        effect[3].SetActive(true);
        effect[4].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true).TimeScale = 0.5f;
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true).TimeScale = 0.5f;
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            StartCoroutine(IETap(tap - 1));
        }
    }

    IEnumerator IETap(int i)
    {
        HintStep6.waitHint = -5f;
        HintStep6.ins.hand.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        item[i].transform.DOMove(way[1].position, 1f).SetEase(Ease.Linear);
        item[i].transform.SetParent(bone4);
        FindObjectOfType<AudioManager>().PlaySound("TapStep9", false);
        yield return new WaitForSeconds(1f);
        skeletonAnimation.AnimationState.ClearTrack(1);
        skeletonAnimation.AnimationState.SetAnimation(0, action1, false).TimeScale = 0.5f;
        yield return new WaitForSeconds(1.333f * 2f);
        item[i].GetComponent<SpriteRenderer>().sortingOrder = 6;
        item[i].transform.SetParent(island);
        item[i].transform.DOMove(way[2].position, 1f).SetEase(Ease.Linear);
        skeletonAnimation.AnimationState.SetAnimation(0, action2, true).TimeScale = 0.5f;
        yield return new WaitForSeconds(1f);
        FindObjectOfType<AudioManager>().PlaySound("DropStep9", false);
        item[i].transform.DORotate(itemEndPos[i].transform.eulerAngles, 1.333f * 1.5f);
        item[i].transform.DOMove(way[3].position, 1.333f * 1.2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(1.333f * 1.2f);
        FindObjectOfType<AudioManager>().PlaySound("DropStep9", false);
        skeletonAnimation.AnimationState.SetAnimation(0, action3, false);
        item[i].transform.DOScale(itemEndPos[i].transform.localScale, 2f);
        item[i].transform.DOMove(itemEndPos[i].transform.position, 2f).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.75f);
        item[i].GetComponent<SpriteRenderer>().sortingOrder = itemEndPos[i].GetComponent<SpriteRenderer>().sortingOrder;
        yield return new WaitForSeconds(1.25f);
        FindObjectOfType<AudioManager>().PlaySound("ChamDat", false);
        HintStep6.waitHint = -5f;
        HintStep6.ins.hand.SetActive(false);
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true).TimeScale = 0.5f;
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true).TimeScale = 0.5f;
        GetComponent<BoxCollider2D>().enabled = true;
        tap++;
        if (tap == 4)
        {
            StartCoroutine(IEEnd());
        }

    }

    IEnumerator IEEnd()
    {
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        transform.DOMoveX(transform.position.x - 9f, 5.5f);
        yield return new WaitForSeconds(4.5f);
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("RuaThuyen");
    }
}
