﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;
using UnityEngine.SceneManagement;

public class Tau4Step8 : MonoBehaviour
{
    public static Tau4Step8 ins;
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string dump;
    [SerializeField] Transform ong;
    [SerializeField] GameObject[] catInSea;
    [SerializeField] GameObject[] catInShip;
    [SerializeField] GameObject[] effectSand;
    [SerializeField] GameObject solidBlack;

    public int tap;
    float startSwipe;
    float endSwipe;

    void Start()
    {
        ins = this;
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < effectSand.Length; i++)
        {
            effectSand[i].SetActive(false);
        }
        for (int i = 0; i < catInSea.Length; i++)
        {
            catInSea[i].transform.localScale = Vector3.zero;
        }
        tap = 1;
        StartCoroutine(IEStart());
    }

    private void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            startSwipe = Input.mousePosition.y;
            HintStep8.ins.waitHint = -5f;
            HintStep8.ins.hand.SetActive(false);
        }
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            HintStep8.ins.waitHint = -5f;
            HintStep8.ins.hand.SetActive(false);
            endSwipe = Input.mousePosition.y;
            if (tap == 1)
            {
                StartCoroutine(IETap1());
            }
            else if (tap == 2)
            {
                StartCoroutine(IETap2());
            }
            else if (tap == 3 && startSwipe - endSwipe > 0)
            {
                StartCoroutine(IETap3());
            }
        }
    }

    IEnumerator IEStart()
    {
        ong.localScale = Vector3.zero;
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOMoveX(transform.position.x + 9.5f, 6f);
        effectSand[2].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        yield return new WaitForSeconds(6f);
        ong.DOMoveX(ong.position.x + 4f, 3f);
        ong.DOScale(new Vector3(1f, 1f, 1f), 3f);
        effectSand[2].GetComponent<ParticleSystem>().Stop();
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        skeletonAnimation.AnimationState.SetAnimation(1, mix, true).TimeScale = 0.5f;
        GetComponent<BoxCollider2D>().enabled = true;
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    IEnumerator IETap1()
    {
        FindObjectOfType<AudioManager>().PlaySound("DoCat", true);
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, dump, true);
        yield return new WaitForSeconds(0.5f);
        effectSand[0].SetActive(true);
        yield return new WaitForSeconds(1.5f);
        effectSand[1].SetActive(true);
        catInShip[0].transform.DOScale(catInShip[0].transform.localScale - new Vector3(-0.328571f, 0.328571f, 0f), 7.5f);
        catInShip[1].transform.DOScale(catInShip[1].transform.localScale - new Vector3(-0.328571f, 0.328571f, 0f), 7.5f);
        catInSea[0].transform.DOScale(new Vector3(1f, 1f, 1f), 7.5f);
        yield return new WaitForSeconds(7.5f);
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        effectSand[0].GetComponent<ParticleSystem>().Stop();
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("DoCat"));
        effectSand[1].GetComponent<ParticleSystem>().Stop();
        tap++;
        GetComponent<BoxCollider2D>().enabled = true;
    }
    IEnumerator IETap2()
    {
        FindObjectOfType<AudioManager>().PlaySound("DoCat", true);
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, dump, true);
        yield return new WaitForSeconds(0.5f);
        effectSand[0].GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(1.5f);
        effectSand[1].GetComponent<ParticleSystem>().Play();
        catInShip[0].transform.DOScale(catInShip[0].transform.localScale - new Vector3(-1.1f, 1.1f, 0f), 7.5f);
        catInShip[1].transform.DOScale(catInShip[1].transform.localScale - new Vector3(-1.1f, 1.1f, 0f), 7.5f);
        catInSea[1].transform.DOScale(new Vector3(1f, 1f, 1f), 7.5f);
        yield return new WaitForSeconds(7.5f);
        skeletonAnimation.AnimationState.SetAnimation(0, idle, true);
        effectSand[0].GetComponent<ParticleSystem>().Stop();
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("DoCat"));
        ong.DOMoveX(ong.position.x - 4f, 3f);
        ong.DOScale(Vector3.zero, 3f);
        Destroy(catInShip[0]);
        Destroy(catInShip[1]);
        GetComponent<BoxCollider2D>().enabled = true;
        tap++;
        effectSand[1].GetComponent<ParticleSystem>().Stop();
    }
    IEnumerator IETap3()
    {
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        GetComponent<BoxCollider2D>().enabled = false;
        effectSand[2].GetComponent<ParticleSystem>().Play();
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 14f, 10f);
        yield return new WaitForSeconds(4f);
        tap++;
        yield return new WaitForSeconds(2f);
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("RuaThuyen");
    }
}
