﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class Tau7Step8 : MonoBehaviour
{
    public SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string move;
    bool end;

    void Start()
    {
        end = true;
        StartCoroutine(IEStart());
    }

    void Update()
    {
        if (end && transform.GetChild(0).childCount <= 1)
        {
            end = false;
            skeletonAnimation.AnimationState.ClearTrack(1);
            transform.DOMoveX(transform.position.x + 7.5f, 7f);
        }
    }

    IEnumerator IEStart()
    {
        yield return new WaitForSeconds(3f);
        transform.DOMoveX(transform.position.x - 7.5f, 7f);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        yield return new WaitForSeconds(7f);
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true).TimeScale = 0.5f;
    }
}
