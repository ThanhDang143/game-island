﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class Ong : MonoBehaviour
{
    public SkeletonAnimation skeletonAnimation;
    [SerializeField] [SpineBone()] private string boneTarget;
    [SerializeField] private Bone bone;
    [SerializeField] GameObject target;

    void Start()
    {
        bone = skeletonAnimation.Skeleton.FindBone(boneTarget);
        skeletonAnimation.UpdateLocal += SkeletonAnimation_UpdateLocal;
    }

    private void SkeletonAnimation_UpdateLocal(ISkeletonAnimation animated)
    {
        var localPosition = transform.InverseTransformPoint(target.transform.position + new Vector3(0f, 0.3f, 0f));
        bone.SetLocalPosition(localPosition);
    }
}
