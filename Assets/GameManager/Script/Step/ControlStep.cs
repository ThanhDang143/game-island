﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlStep : BaseStep
{
    // Start is called before the first frame update
    [SerializeField] BaseStep[] step;

    void Start()
    {
        for (int i = 0; i < step.Length; i++)
        {
            if (ControlGame.ins.idStep == i)
            {
                step[i].OnShow();
            }
            else
            {
                step[i].gameObject.SetActive(false);
            }
        }
        ControlGame.ins.cam = Camera.main;
    }
}
