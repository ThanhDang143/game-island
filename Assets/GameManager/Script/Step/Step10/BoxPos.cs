﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class BoxPos : MonoBehaviour
{
    [SerializeField] GameObject[] building;
    [SerializeField] GameObject tau3;
    [SerializeField] GameObject solidBlack;
    int selectedBuilding;
    void Start()
    {
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < building.Length; i++)
        {
            building[i].SetActive(false);
        }
        for (int i = 0; i < building[0].transform.childCount - 1; i++)
        {
            building[0].transform.GetChild(i).GetComponent<SpriteRenderer>().DOFade(0f, 0f);
        }
        building[0].transform.GetChild(building[0].transform.childCount - 1).gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Box"))
        {
            HintStep10.waitHint = 0f;
            StartCoroutine(IEOnTrigger(other));
        }
    }

    IEnumerator IEOnTrigger(Collider2D other)
    {
        other.transform.DOMoveY(other.transform.position.y - Random.Range(0.95f, 1.25f), 0.75f).SetEase(Ease.Linear);
        other.transform.DORotate(Vector3.zero, 0.75f);
        other.transform.SetParent(transform);
        yield return new WaitForSeconds(0.75f);
        FindObjectOfType<AudioManager>().PlaySound("ChamDat", false);
        other.transform.GetChild(0).gameObject.SetActive(true);
        other.GetComponent<BoxCollider2D>().enabled = false;
    }

    private void OnMouseDown()
    {
        HintStep10.waitHint = -5000f;
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            if (Tau3Step10.ins.tap == 4)
            {

                StartCoroutine(IEOnMouseUp());
            }
        }
    }

    IEnumerator IEOnMouseUp()
    {
        FindObjectOfType<AudioManager>().PlaySound("ShowBuilding", false);
        for (int i = 0; i < building.Length; i++)
        {
            if (building[i].name == MoveScreen.ins.selectedLevel)
            {
                selectedBuilding = i;
            }
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetChild(1).gameObject.SetActive(true);
            transform.GetChild(i).GetComponent<SpriteRenderer>().DOFade(0f, 0.75f);
        }
        building[selectedBuilding].SetActive(true);
        building[selectedBuilding].transform.GetChild(building[selectedBuilding].transform.childCount - 1).gameObject.SetActive(true);
        for (int i = 0; i < building[selectedBuilding].transform.childCount - 1; i++)
        {
            building[selectedBuilding].transform.GetChild(i).GetComponent<SpriteRenderer>().DOFade(1f, 0.75f);
        }
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        yield return new WaitForSeconds(1f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        tau3.transform.DOMoveX(tau3.transform.position.x - 8f, 5.5f);
        yield return new WaitForSeconds(3.5f);
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("RuaThuyen");
    }
}
