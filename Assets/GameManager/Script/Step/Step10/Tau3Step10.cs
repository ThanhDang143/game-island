﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class Tau3Step10 : MonoBehaviour
{
    public static Tau3Step10 ins;
    [SerializeField] GameObject[] view;
    [SerializeField] GameObject[] effect;
    [SerializeField] Transform[] box;
    public SkeletonAnimation skeletonAnimation;
    [SerializeField] Transform target;
    [SerializeField] [SpineBone()] private string boneTarget;
    [SerializeField] private Bone bone;
    [SpineAnimation()] public string idle;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string pull1;
    [SpineAnimation()] public string pull2;
    [SpineAnimation()] public string pull3;
    public int tap;

    void Start()
    {
        ins = this;
        for (int i = 0; i < view.Length; i++)
        {
            view[i].SetActive(false);
        }
        for (int i = 0; i < effect.Length; i++)
        {
            effect[i].SetActive(false);
        }
        for (int i = 1; i < box.Length; i++)
        {
            box[i].GetComponent<SpriteRenderer>().DOFade(0f, 0f);
            box[i].GetChild(0).gameObject.SetActive(false);
            box[i].GetChild(1).gameObject.SetActive(false);
            box[i].gameObject.SetActive(false);
        }
        box[0].GetComponent<BoxCollider2D>().enabled = false;
        box[1].GetComponent<SpriteRenderer>().DOFade(1f, 0f);
        box[1].gameObject.SetActive(true);
        effect[0].SetActive(true);
        tap = 1;
        StartCoroutine(IEStart());
        bone = skeletonAnimation.Skeleton.FindBone(boneTarget);
        skeletonAnimation.UpdateLocal += SkeletonAnimation_UpdateLocal;
    }

    private void SkeletonAnimation_UpdateLocal(ISkeletonAnimation animated)
    {
        var localPosition = transform.InverseTransformPoint(target.position);
        bone.SetLocalPosition(localPosition);
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            HintStep10.waitHint = 0f;
            StartCoroutine(IETap(tap));
        }
    }

    IEnumerator IEStart()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        effect[1].SetActive(true);
        effect[2].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 9.5f, 5.5f);
        yield return new WaitForSeconds(5.5f);
        GetComponent<BoxCollider2D>().enabled = true;
        HintStep10.waitHint = 0f;
        HintStep10.ins.hand.SetActive(false);
        effect[2].GetComponent<ParticleSystem>().Stop();
        effect[3].SetActive(true);
        effect[4].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true);
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }

    IEnumerator IETap(int i)
    {
        HintStep10.waitHint = 0f;
        HintStep10.ins.hand.SetActive(false);
        effect[5].SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        skeletonAnimation.AnimationState.ClearTrack(1);
        transform.DOMove(new Vector3(transform.position.x + 2.75f - i, transform.position.y + 0.5f, transform.position.z), 2f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        yield return new WaitForSeconds(2f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
        skeletonAnimation.AnimationState.SetAnimation(0, pull1, false);
        yield return new WaitForSeconds(2.667f);
        box[0].GetComponent<BoxCollider2D>().enabled = true;
        skeletonAnimation.AnimationState.SetAnimation(0, pull2, true);
        target.transform.DOMove(new Vector3(target.transform.position.x + 0.1f, target.transform.position.y - 1.25f, target.transform.position.z), 2f);
        yield return new WaitForSeconds(2f);
        target.transform.DOMove(new Vector3(target.transform.position.x - 0.1f, target.transform.position.y + 1.25f, target.transform.position.z), 2f);
        yield return new WaitForSeconds(2f);
        box[0].GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, pull3, false);
        yield return new WaitForSeconds(2.667f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        transform.DOMove(new Vector3(transform.position.x - 2.75f + i, transform.position.y - 0.5f, transform.position.z), 2f);
        skeletonAnimation.AnimationState.SetAnimation(0, move, false);
        yield return new WaitForSeconds(2.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
        HintStep10.waitHint = 0f;
        HintStep10.ins.hand.SetActive(false);
        skeletonAnimation.AnimationState.SetAnimation(0, mix, true);
        skeletonAnimation.AnimationState.SetAnimation(1, idle, true);
        if (i + 1 < box.Length)
        {
            FindObjectOfType<AudioManager>().PlaySound("ChamDat", false);
            effect[5].SetActive(true);
            box[i + 1].GetComponent<SpriteRenderer>().DOFade(1f, 0.8f);
            box[i + 1].gameObject.SetActive(true);
        }
        tap++;
        GetComponent<BoxCollider2D>().enabled = true;
        if (tap == 4)
        {
            GetComponent<BoxCollider2D>().enabled = false;
            box[0].GetComponent<BoxCollider2D>().enabled = true;
            box[0].GetComponent<BoxCollider2D>().isTrigger = false;

            for (int b = 1; b < box.Length; b++)
            {
                StartCoroutine(IEZoom(box[b], Random.Range(0.5f, 1f)));
            }
        }
    }

    IEnumerator IEZoom(Transform obj, float time)
    {
        while (true)
        {
            obj.DOScale(new Vector3(0.85f, 0.85f, 0.85f), time);
            yield return new WaitForSeconds(time);
            obj.DOScale(new Vector3(0.75f, 0.75f, 0.75f), time);
            yield return new WaitForSeconds(time);
        }
    }
}
