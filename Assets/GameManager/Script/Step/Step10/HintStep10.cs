﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HintStep10 : MonoBehaviour
{
    public static HintStep10 ins;
    public GameObject hand;
    public GameObject box;
    private Animator anim;
    public AnimationClip tay;
    public AnimationClip tayx;
    public static float waitHint = 0;

    void Start()
    {
        ins = this;
        waitHint = 0;
        anim = hand.GetComponent<Animator>();
        hand.SetActive(false);
        ControlGame.ins.hideHint = () => hand.SetActive(false);
    }

    void Update()
    {
        waitHint += Time.deltaTime;
        if (waitHint > 10f)
        {
            if (Tau3Step10.ins.tap == 4)
            {
                StartCoroutine(IETapHint(box));
            }
            else
            {
                StartCoroutine(IETapHint(gameObject));
            }
            waitHint = 5f;
        }
    }

    public IEnumerator IETapHint(GameObject tapObj)
    {
        if (Tau3Step10.ins.tap == 4)
        {
            hand.transform.position = tapObj.transform.position - new Vector3(-0.35f, 0.5f, 0f);
        }
        else
        {
            hand.transform.position = tapObj.transform.position - new Vector3(-0.35f, -0.5f, 0f);
        }
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.25f);
        yield return new WaitForSeconds(1f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        hand.SetActive(false);
    }
}
