﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MongStep6 : MonoBehaviour
{
    [SerializeField] GameObject soi;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("May") || other.CompareTag("Song")) return;
        other.gameObject.transform.SetParent(transform);
        soi.transform.position += new Vector3(0f, 0.0035f, 0f);
        StartCoroutine(IEDetroySoi(other.gameObject));
    }

    IEnumerator IEDetroySoi(GameObject soi)
    {
        soi.GetComponent<SpriteRenderer>().DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        Destroy(soi);
    }
}
