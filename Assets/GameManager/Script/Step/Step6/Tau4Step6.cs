﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Spine;
using Spine.Unity;

public class Tau4Step6 : MonoBehaviour
{
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SpineAnimation()] public string mix;
    [SpineAnimation()] public string move;
    [SpineAnimation()] public string oneone;
    [SpineAnimation()] public string onethree;
    [SpineAnimation()] public string twoone;
    [SpineAnimation()] public string twothree;
    [SerializeField] GameObject[] effect;
    [SerializeField] GameObject solidBlack;
    int tap = 1;

    // Start is called before the first frame update
    void Start()
    {
        solidBlack.GetComponent<SpriteRenderer>().DOFade(0, 0f);
        for (int i = 0; i < effect.Length; i++)
        {
            effect[i].SetActive(false);
        }
        StartCoroutine(IEStart());
    }

    private void OnMouseUp()
    {
        if (Time.timeScale != 0)
        {
            HintStep6.waitHint = -5f;
            HintStep6.ins.hand.SetActive(false);
            if (tap == 1)
            {
                tap++;
                StartCoroutine(IETap1());
                return;
            }
            if (tap == 2)
            {
                StartCoroutine(IETap2());
                return;
            }
        }
    }

    IEnumerator IETap1()
    {
        HintStep6.waitHint = -5f;
        HintStep6.ins.hand.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        transform.DOMoveX(transform.position.x + 3.5f, 5f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        yield return new WaitForSeconds(5.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
        transform.DORotate(new Vector3(0f, 0f, -3.5f), 5f);
        skeletonAnimation.AnimationState.SetAnimation(0, oneone, false).TimeScale = 0.5f;
        FindObjectOfType<AudioManager>().PlaySound("DoDa", false);
        yield return new WaitForSeconds(6.5f);
        transform.DORotate(new Vector3(0f, 0f, 0f), 2f);
        skeletonAnimation.AnimationState.SetAnimation(0, onethree, false).TimeScale = 0.5f;
        yield return new WaitForSeconds(2f);
        GetComponent<BoxCollider2D>().enabled = true;
        HintStep6.waitHint = -5f;
        HintStep6.ins.hand.SetActive(false);
    }
    IEnumerator IETap2()
    {
        HintStep6.waitHint = -2000f;
        HintStep6.ins.hand.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOMoveX(transform.position.x - 2f, 4.5f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        yield return new WaitForSeconds(2.5f);
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
        skeletonAnimation.AnimationState.SetAnimation(0, twoone, false).TimeScale = 0.5f;
        FindObjectOfType<AudioManager>().PlaySound("DoDa", false);
        yield return new WaitForSeconds(6.5f);
        skeletonAnimation.AnimationState.SetAnimation(0, twothree, false).TimeScale = 0.5f;
        HintStep6.waitHint = -500f;
        yield return new WaitForSeconds(5.5f);
        effect[1].GetComponent<ParticleSystem>().Play();
        effect[2].GetComponent<ParticleSystem>().Stop();
        effect[3].GetComponent<ParticleSystem>().Stop();
        transform.DOMoveX(transform.position.x + 13f, 10f);
        FindObjectOfType<AudioManager>().PlaySound("Move", true);
        yield return new WaitForSeconds(8f);
        solidBlack.GetComponent<SpriteRenderer>().DOFade(1, 1f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("RuaThuyen");
    }

    IEnumerator IEStart()
    {
        effect[0].SetActive(true);
        effect[1].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, move, true);
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOMoveX(transform.position.x + 7.5f, 4f);
        yield return new WaitForSeconds(4f);
        effect[1].GetComponent<ParticleSystem>().Stop();
        effect[2].SetActive(true);
        effect[3].SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(1, mix, true);
        GetComponent<BoxCollider2D>().enabled = true;
        StartCoroutine(FindObjectOfType<AudioManager>().IEStopSound("Move"));
    }
}
