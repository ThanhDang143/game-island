﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soi : MonoBehaviour
{
    public GameObject soi;

    [SerializeField] GameObject soiParent;

    public Sprite[] soiSprite;

    void Start()
    {
        for (int i = 0; i <= 150; i++)
        {
            SpawnSoi();
        }
    }

    private void SpawnSoi()
    {
        GameObject n = Instantiate(soi, new Vector3(-10.75f, 1f, -0.75f), Quaternion.identity, soiParent.transform);
        n.GetComponent<SpriteRenderer>().sprite = soiSprite[Random.Range(0, soiSprite.Length)];
    }
}
