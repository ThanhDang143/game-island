﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HintStep6 : MonoBehaviour
{
    public static HintStep6 ins;
    public GameObject hand;
    private Animator anim;
    public AnimationClip tay;
    public AnimationClip tayx;
    public static float waitHint = 0;

    void Start()
    {
        ins = this;
        waitHint = 0;
        anim = hand.GetComponent<Animator>();
        ControlGame.ins.hideHint = () => hand.SetActive(false);
        hand.SetActive(false);
    }

    void Update()
    {
        waitHint += Time.deltaTime;

        if (waitHint > 7f)
        {
            StartCoroutine(IETapHint(gameObject));
            waitHint = 2f;
        }
    }

    public IEnumerator IETapHint(GameObject tapObj)
    {
        hand.transform.position = tapObj.transform.position - new Vector3(-0.35f, -0.5f, 0f);
        hand.SetActive(true);
        anim.Play(tay.name);
        hand.GetComponent<SpriteRenderer>().DOFade(1, 0.25f);
        yield return new WaitForSeconds(1f);
        anim.Play(tayx.name);
        yield return new WaitForSeconds(0.75f);
        hand.GetComponent<SpriteRenderer>().DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        hand.SetActive(false);
    }
}
